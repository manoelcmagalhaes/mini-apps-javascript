const http = require('http');
const fs = require('fs');
var url = require('url');
const concat = require('concat-stream');
const qs = require('querystring');
http.createServer((req, res) => {
    var pathname = url.parse(req.url).pathname;
    if (pathname === '/nd') {
        fs.readFile('page/nd.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/index.js') {
        fs.readFile('page/scripts/index.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/tanque.js') {
        fs.readFile('page/scripts/tanque.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/entrada.js') {
        fs.readFile('page/scripts/entrada.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/calc_w3c.js') {
        fs.readFile('page/scripts/calc_w3c.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/calc_mod.css') {
        fs.readFile('page/styles/calc_mod.css', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/css'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/keypress') {
        fs.readFile('page/keypress.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/poligono') {
        fs.readFile('page/poligono.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/tanque') {
        fs.readFile('page/tanque.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/cores') {
        fs.readFile('page/cores.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/calc_v2.js') {
        fs.readFile('page/scripts/calc_v2.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/piramide') {
        fs.readFile('page/piramide.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/primos') {
        fs.readFile('page/primos.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/caixa') {
        fs.readFile('page/caixa.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/triangulo') {
        fs.readFile('page/triangulo.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/captura') {
        fs.readFile('page/captura.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/joystick') {
        fs.readFile('page/joystick.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/piramide.js') {
        fs.readFile('page/scripts/piramide.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/joystick.js') {
        fs.readFile('page/scripts/joystick.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/cria_arrays_game.js') {
        fs.readFile('page/scripts/cria_arrays_game.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/tanquev2.js') {
        fs.readFile('page/scripts/tanquev2.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/recursos.js') {
        fs.readFile('page/scripts/recursos.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/resistencia.js') {
        fs.readFile('page/scripts/resistencia.js', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/login') {
        fs.readFile('page/login.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/estilo.css') {
        fs.readFile('page/styles/estilo.css', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/css'});
            res.write(data);
            return res.end();
        });
    } else if (pathname === '/processamento') {
        console.log('redirecionou!');
        res.statusCode = 302; //O código é necessário
        res.setHeader('Location', '/');
        return res.end();
    } else if (pathname === '/resistencia') {
        fs.readFile('page/resistencia.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    } else {
        fs.readFile('page/index.html', function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            return res.end();
        });
    }
}).listen(9292, "");
