var x, ope, valor1, valor2, valorfinal;
function display() {
    var numDisplay = document.getElementById("calc-display").innerHTML;
    var numDisplayTmp;
    if (numDisplay === "0") {
        document.getElementById("calc-display").innerHTML = x;
    } else {
        numDisplayTmp = numDisplay + x;
        document.getElementById("calc-display").innerHTML = numDisplayTmp;
    }
}
function operador() {
    if (ope === "sen" || ope === "tan" || ope === "cos") {
        valor1 = parseFloat(document.getElementById("calc-display").innerHTML);
        calculo();
        document.getElementById("calc-display").innerHTML = valorfinal;
    } else {
        valor1 = parseFloat(document.getElementById("calc-display").innerHTML);
        reset("tela");
    }
}
function resultado() {
    valor2 = parseFloat(document.getElementById("calc-display").innerHTML);
    calculo();
    document.getElementById("calc-display").innerHTML = valorfinal;
}
function calculo() {
    if (ope === "+") {
        valorfinal = valor1 + valor2;
    } else if (ope === "-") {
        valorfinal = valor1 - valor2;
    } else if (ope === "*") {
        valorfinal = valor1 * valor2;
    } else if (ope === "/") {
        valorfinal = valor1 / valor2;
    } else if (ope === "^") {
        valorfinal = Math.pow(valor1, valor2);
    } else if (ope === "%") {
        valorfinal = ((valor2 / 100) * valor1);
    } else if (ope === "sen") {
        var valorRad = valor1 * (Math.PI / 180);
        valorfinal = Math.sin(valorRad);
    } else if (ope === "cos") {
        var valorRad = valor1 * (Math.PI / 180);
        valorfinal = Math.cos(valorRad);
    } else if (ope === "tan") {
        if (valor1 === 90 || valor1 === 270) {
            valorfinal = "não existe";
        }else if (valor1 === 180 || valor1 === 360){
            valorfinal = 0;
        }else {
            var valorRad = valor1 * (Math.PI / 180);
            valorfinal = Math.tan(valorRad);
        }
    }
}
function virgula() {
    var numDisplay = document.getElementById("calc-display").innerHTML;
    var virgulaDisplay = numDisplay.indexOf(".");
    var numDisplayTmp, numDisplaySplit;
    if (virgulaDisplay === -1) {
        numDisplayTmp = numDisplay + ".";
        document.getElementById("calc-display").innerHTML = numDisplayTmp;
    } else {
        numDisplaySplit = numDisplay.split(".");
        numDisplayTmp = numDisplaySplit[0] + numDisplaySplit[1] + ".";
        document.getElementById("calc-display").innerHTML = numDisplayTmp;
    }
}
function reset(tipo) {
    if (tipo === "tela") {
        document.getElementById("calc-display").innerHTML = "0";
    } else if (tipo === "var") {
        x = undefined;
        ope = undefined;
        valor1 = undefined;
        valor2 = undefined;
        valorfinal = undefined;
    } else if (tipo === "geral") {
        x = undefined;
        ope = undefined;
        valor1 = undefined;
        valor2 = undefined;
        valorfinal = undefined;
        document.getElementById("calc-display").innerHTML = "0";
    }
}
function backspace() {
    var cnt_tela = document.getElementById("calc-display").innerHTML;
    var cnt_tela_tmp;
    if (cnt_tela !== "0") {
        if (cnt_tela.length === 1) {
            document.getElementById("calc-display").innerHTML = "0";
        } else {
            cnt_tela_tmp = cnt_tela[0];
            for (var i = 1; i < cnt_tela.length - 1; i++) {
                cnt_tela_tmp = cnt_tela_tmp + cnt_tela[i];
            }
            document.getElementById("calc-display").innerHTML = cnt_tela_tmp;
        }
    }
}
function cientifica() {
    var itensCientifica = document.getElementsByClassName("cientifica");
    var qntItensCientifica = itensCientifica.length;
    var botoesCientifica = document.getElementsByClassName("cien");
    var qntBotoesCientifica = botoesCientifica.length;
    var cntb;
    var cnt;
    var estadoItem = document.getElementById("item_de_teste").style.display;
    if ((estadoItem === "none") || (estadoItem === "")) {
        document.getElementById("calc-display").style.width = "30%";
        for (cnt = 0; cnt < qntItensCientifica; cnt++) {
            itensCientifica[cnt].style.display = "inline";
        }
        for (cntb = 0; cntb < qntBotoesCientifica; cntb++) {
            botoesCientifica[cntb].style.width = "10%";
        }
    } else {
        document.getElementById("calc-display").style.width = "20%";
        for (cnt = 0; cnt < qntItensCientifica; cnt++) {
            itensCientifica[cnt].style.display = "none";
        }
        for (cntb = 0; cntb < qntBotoesCientifica; cntb++) {
            botoesCientifica[cntb].style.width = "5%";
        }
    }
}
document.addEventListener('keydown', function (event) {
    if (event.keyCode === 96) {
        x = 0;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 97) {
        x = 1;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 98) {
        x = 2;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 99) {
        x = 3;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 100) {
        x = 4;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 101) {
        x = 5;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 102) {
        x = 6;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 103) {
        x = 7;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 104) {
        x = 8;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 105) {
        x = 9;
        display();
        document.getElementById("botao_" + x).disabled = true;
    } else if (event.keyCode === 110) {
        x = ",";
        virgula();
        document.getElementById("botao_ponto").disabled = true;
    } else if (event.keyCode === 111) {
        ope = "/";
        operador();
        document.getElementById("botao_divisao").disabled = true;
    } else if (event.keyCode === 106) {
        ope = "*";
        operador();
        document.getElementById("botao_multiplicacao").disabled = true;
    } else if (event.keyCode === 109) {
        ope = "-";
        operador();
        document.getElementById("botao_subtracao").disabled = true;
    } else if (event.keyCode === 107) {
        ope = "+";
        operador();
        document.getElementById("botao_soma").disabled = true;
    } else if (event.keyCode === 111) {
        ope = "/";
        operador();
        document.getElementById("botao_divisao").disabled = true;
    } else if (event.keyCode === 194) {
        ope = ",";
        operador();
        document.getElementById("botao_ponto").disabled = true;
    } else if (event.keyCode === 13) {
        eq = "=";
        resultado();
        document.getElementById("botao_igual").disabled = true;
    } else if (event.keyCode === 27) {
        reset("geral");
        document.getElementById("botao_limpar").disabled = true;
    } else if (event.keyCode === 8) {
        backspace();
    }
});
document.addEventListener('keyup', function (event) {
    if (event.keyCode === 96) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 97) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 98) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 99) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 100) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 101) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 102) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 103) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 104) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 105) {
        document.getElementById("botao_" + x).disabled = false;
    } else if (event.keyCode === 110) {
        document.getElementById("botao_ponto").disabled = false;
    } else if (event.keyCode === 111) {
        document.getElementById("botao_divisao").disabled = false;
    } else if (event.keyCode === 106) {
        document.getElementById("botao_multiplicacao").disabled = false;
    } else if (event.keyCode === 109) {
        document.getElementById("botao_subtracao").disabled = false;
    } else if (event.keyCode === 107) {
        document.getElementById("botao_soma").disabled = false;
    } else if (event.keyCode === 111) {
        document.getElementById("botao_divisao").disabled = false;
    } else if (event.keyCode === 194) {
        document.getElementById("botao_ponto").disabled = false;
    } else if (event.keyCode === 13) {
        document.getElementById("botao_igual").disabled = false;
    } else if (event.keyCode === 27) {
        document.getElementById("botao_limpar").disabled = false;
    }
});