document.addEventListener("load", mudaTitulo());
var validaNome, validaNumero, validaEmail, nomeRec, foneRec, emailRec;
function msg() {
    var pag = null;
    pag = document.getElementById("tituloNavbar").innerHTML.split(" ");
    if (pag[0] === "Keypress") {
        alert("Teste de captura de teclado com uso do atributo 'keyDown'");
    } else if (pag[0] === "Nd") {
        alert("Teste do do metodo 'this' para captura de dados");
    } else if (pag[0] === "Index") {
        alert("Teste do metodo eventListener");
    } else if (pag[0] === "Primos") {
        alert("O script recebe o conteudo da caixa input através de um getElementById, transforma o conteudo em float através de um metodo parseFloat e inicia dois laços for, o primeiro realiza a checagem individual de cada numero entre 1 e o valor inserido, o segundo realiza o calculo mátematico e verifica se o numero é divisivel por 1 e por ele mesmo, caso seja ele retorna a mensagem de aviso.");
    } else if (pag[0] === "???") {
        alert("O script recebe o conteudo da caixa input através de um getElementById e armazena numa variavel chamada 'entrada', após o recebimento ele analiza a variavel e verifica se o conteudo dela está entre 1 e 10, caso não esteja ele envia uma caixa de alerta avisando que a condição é incompativel, caso esteja ele limpa o conteudo da tag div destinada a exibição da piramide, ativa esse botão de help e inicia a função que cria a piramide, a função consiste de um laço for, dentro há um comando if que verifica se o contador está em 1, se estiver ele armazena 1 em formato string na variavel tmp, caso o contador não esteja mais em 1 ele vai concatenar o valor anterior do contador i com o atual em forma de string, após o if é feita a conversão do conteudo da variavel tmp de string para float, o resultado é armazenado numa nova variavel chamada tmpflo, o calculo é feito multiplicando o conteudo da variavel tmpflo por 8 e somando o conteudo do contador ao produto da multiplicação, o resultado é armazenado na variavel 'outrolado', ao fim o conteudo é concatenado e adicionado a página atraves do metodo .innerHTML, a cada volta do laço for uma nova linha é adicionada.");
    } else if (pag[0] === "Poligono") {
        alert(" - Página em construção -\nO objetivo dessa página é gerar o código necessario para a criação de um poligono, ela gera um poligono aleatoriamente atráves da inserção da quantidade de pontos desejados e cria automaticamente as caixas para entrada dos valores x e y de cada ponto criado");
    } else if (pag[0] === "Joystick") {
        alert(" - Página em construção -\nO objetivo dessa página é servir de teste para o uso de entradas do teclado na manipulação de gráficos svg");
    } else if (pag[0] === "Caixa") {
        alert("Pasta para de testes de input");
    } else if (pag[0] === "Gerador") {
        alert(" - Página em aperfeiçoamento -\nO objetivo desta página é gerar código para funções condicionais extensas do Excel e do Report Builder.");
    } else if (pag[0] === "Tanque") {
        alert(" - Página em aperfeiçoamento -\nO objetivo desta página é servir de apoio para o estudo de rotação de objetos inseridos em planos cartesianos");
    } else if (pag[0] === "Triangulo") {
        alert("A pagina tem um triangulo que pode ser alterado através das caixas de input, é necessário inserir as cordenadas para modificar a forma do triangulo, após o fechamento desse aviso as caixas de input serão preenchidos com os valores que formam o triangulo original.");
        document.getElementById('valor1a').value = 100;
        document.getElementById('valor1b').value = 0;
        document.getElementById('valor2a').value = 100;
        document.getElementById('valor2b').value = 100;
        document.getElementById('valor3a').value = 0;
        document.getElementById('valor3b').value = 100;
        triangula();
    } else {
        alert("Página não configurada");
    }
}
function campoFone() {
    var nome = document.getElementById('campo-fone').value;
    var pNome = nome.indexOf("(");
    if (pNome !== -1) {
        document.getElementById('campo-fone').value = nome;
        if (document.getElementById('campo-fone').value.length === 3) {
            document.getElementById('campo-fone').value = nome + ") ";
        }
        if (document.getElementById('campo-fone').value.length === 10) {
            document.getElementById('campo-fone').value = nome + "-";
        }
        if (document.getElementById('campo-fone').value.length > 14) {
            validaNumero = false;
            document.getElementById('campo-fone').style.color = "red";
        } else {
            document.getElementById('campo-fone').style.color = "black";
            validaNumero = true;
        }
    } else {
        document.getElementById('campo-fone').value = "(" + nome;
    }
}
function campoEmail() {
    var email = document.getElementById('campo-email').value;
    var pEmail = email.indexOf("@");
    var pEmail2 = email.indexOf(".");
    if (pEmail !== -1 || pEmail2 !== -1) {
        validaEmail = true;
    } else {
        validaEmail = false;
    }
}
function campoNome() {
    var nome = document.getElementById('campo-nome').value;
    if (nome === "") {
        validaNome = false;
    } else {
        validaNome = true;
    }
}
function valida() {
    if (validaNome === false || validaNumero === false || validaEmail === false) {
        alert("Os dados estão incorretos");
    } else if (validaNome === undefined || validaNumero === undefined || validaEmail === undefined) {
        alert("Os dados estão incompletos");
    } else {
        document.getElementById("nome-recebido").value = document.getElementById("campo-nome").value;
        document.getElementById("fone-recebido").value = document.getElementById("campo-fone").value;
        document.getElementById("email-recebido").value = document.getElementById("campo-email").value;
    }
}
function mudaTitulo() {
    var titulo = document.getElementById("tituloNavbar").innerHTML;
    document.title = titulo;
    gerabotoes();
}
function gerabotoes() {
    var botao_help = "<button class='btn btn-outline-dark btn-sm' id='botao-info' onclick='msg()'>\
                        <i class='bi bi-question-lg'></i>\
                    </button>\
                    ";
    var botao_home = "<a href='/index' class='btn btn-outline-dark btn-sm' id='home'>\
                        <i class='bi bi-ui-checks'></i>\
                    </a>\
                    ";
    var botao_calc_or = "<a href='/nd' class='btn btn-outline-dark btn-sm' id='calc'>\
                        <i class='bi bi-calculator'></i>\
                    </a>\
                    ";
    var botao_calc_key = "<a href='/keypress' class='btn btn-outline-dark btn-sm' id='calc2'>\
                        <i class='bi bi-calculator-fill'></i>\
                    </a>\
                    ";
    var botao_primo = "<a href='/primos' class='btn btn-outline-dark btn-sm' id='primo'>\
                        <i class='bi bi-sort-numeric-down-alt'></i>\
                    </a>\
                    ";
    var botao_piramide = "<a href='/piramide' class='btn btn-outline-dark btn-sm' id='piramide'>\
                        <i class='bi bi-caret-up'></i>\
                    </a>\
                    ";
    var botao_caixa = "<a href='/caixa' class='btn btn-outline-dark btn-sm' id='piramide'>\
                        <i class='bi bi-box-seam'></i>\
                    </a>\
                    ";
    var botao_cores = "<a href='/cores' class='btn btn-outline-dark btn-sm' id='cores'>\
                        <i class='bi bi-palette'></i>\
                    </a>\
                    ";
    var botao_tri = "<a href='/triangulo' class='btn btn-outline-dark btn-sm' id='triangulo'>\
                        <i class='bi bi-caret-up-fill'></i>\
                    </a>\
                    ";
    var botao_joy = "<a href='/joystick' class='btn btn-outline-dark btn-sm' id='joystick'>\
                        <i class='bi bi-joystick'></i>\
                    </a>\
                    ";
    var botao_pol = "<a href='/poligono' class='btn btn-outline-dark btn-sm' id='poligono'>\
                        <i class='bi bi-octagon'></i>\
                    </a>\
                    ";
    var botao_tnk = "<a href='/tanque' class='btn btn-outline-dark btn-sm' id='tanque'>\
                        <i class='bi bi-controller'></i>\
                    </a>\
                    ";
    var botao_cam = "<a href='/captura' class='btn btn-outline-dark btn-sm' id='camera'>\
                        <i class='bi bi-camera'></i>\
                    </a>\
                    ";
	var botao_login = "<a href='/login' class='btn btn-outline-dark btn-sm' id='login'>\
                        <i class='bi bi-box-arrow-right'></i>\
                    </a>\
                    ";
	var botao_resistencia = "<a href='/resistencia' class='btn btn-outline-dark btn-sm' id='resistencia'>\
					<i class='bi bi-cpu'></i>\
					</a>\
                    ";
    var botoes = [botao_help, botao_home, botao_calc_or, botao_calc_key, botao_primo, botao_piramide, botao_caixa, botao_cores, botao_tri, botao_joy, botao_pol, botao_tnk, botao_cam, botao_login, botao_resistencia];
    var nomes = ["Help", "Index", "Nd", "Keypress", "Primos", "???", "Caixa", "Gerador", "Triangulo", "Joystick", "Poligono", "Tanque", "Captura", "Login", "Resistencia"];
    var titulo = document.getElementById("tituloNavbar").innerHTML.split(" ");
    for (var i = 0; i < botoes.length; i++) {
        if (titulo[0] !== nomes[i]) {
            document.getElementById("botoes_header").innerHTML += botoes[i];
        }
    }
}
