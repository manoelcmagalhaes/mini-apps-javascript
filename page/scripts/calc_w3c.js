var x, ope, xValor, yValor, resultTela;
function mudaVirgula(){
	var onDisplayNum = document.getElementById("calc-display").innerHTML;
	if(onDisplayNum === "0"){
		document.getElementById("calc-display").innerHTML = "0.";
	}
	else{
		var idVirgula = onDisplayNum.indexOf(".");
		if (idVirgula !== -1){
			var idVirgulaSim = onDisplayNum.split(".");
			var onDisplayNumNovo = idVirgulaSim[0]+idVirgulaSim[1]+".";
			document.getElementById("calc-display").innerHTML = onDisplayNumNovo;
		}
		else{
			coletaValoresXeY();
		}
	}
}
function inicio(){
	//var checaTela = document.getElementById("calc-display").innerHTML;
	var conteudoTela = parseFloat(document.getElementById("calc-display").innerHTML);
	if(resultTela === conteudoTela){
		ope = undefined;
		resultTela = undefined;
		limpaTela();
		coletaValoresXeY();
	}
	else{
		coletaValoresXeY();
	}
}
function igualPressionado(){
	calcOp();
	document.getElementById("calc-display").innerHTML = resultTela;
	limpaVar();
}
function calcOp(){
	if (ope === "+"){
		resultTela = xValor + yValor;
	}
	else if (ope === "-"){
		resultTela = xValor - yValor;
	}
	else if (ope === "*"){
		resultTela = xValor * yValor;
	}
	else if (ope === "/"){
		resultTela = xValor / yValor;
	}
	else if (ope === "^"){
		resultTela = Math.pow(xValor, yValor);
	}
	else if (ope === "%"){
		resultTela = ((yValor/100)*xValor);
	}
}
function coletaValoresXeY(){
	if(ope === undefined){
		if(document.getElementById("calc-display").innerHTML === "0"){
			document.getElementById("calc-display").innerHTML = x;
			xValor = parseFloat(x);
		}
		else{
			var telaAtual = document.getElementById("calc-display").innerHTML;
			var telaNova = telaAtual+x;
			document.getElementById("calc-display").innerHTML = telaNova;
			xValor = parseFloat(telaNova);
		}
	}
	else{
		if(document.getElementById("calc-display").innerHTML === "0"){
			document.getElementById("calc-display").innerHTML = x;
			yValor = parseFloat(x);
		}
		else{
			var telaAtual = document.getElementById("calc-display").innerHTML;
			var telaNova = telaAtual+x;
			document.getElementById("calc-display").innerHTML = telaNova;
			yValor = parseFloat(telaNova);
		}
	}
}
function limpaTela(){
	document.getElementById("calc-display").innerHTML = "0";
}
function limpaTudo(){
	document.getElementById("calc-display").innerHTML = "0";
	resultTela = undefined;
	xValor = undefined;
	yValor = undefined;
	ope = undefined;
	x = undefined;
}
function limpaVar(){
	xValor = undefined;
	yValor = undefined;
	ope = undefined;
	x = undefined;
}
function ativaCien(){
	var itensCientifica = document.getElementsByClassName("cientifica");
	var qntItensCientifica = itensCientifica.length;
	var botoesCientifica = document.getElementsByClassName("cien");
	var qntBotoesCientifica = botoesCientifica.length;
	var cntb;
	var cnt;
	var estadoItem = document.getElementById("item_de_teste").style.display;
	if((estadoItem === "none") || (estadoItem === "")){
		document.getElementById("calc-display").style.width = "30%";
		for (cnt = 0; cnt < qntItensCientifica; cnt++){
			itensCientifica[cnt].style.display="inline";
		}
		for (cntb = 0; cntb < qntBotoesCientifica; cntb++){
			botoesCientifica[cntb].style.width = "10%";
		}
	}
	else{
		document.getElementById("calc-display").style.width = "20%";
		for (cnt = 0; cnt < qntItensCientifica; cnt++){
			itensCientifica[cnt].style.display="none";
		}
		for (cntb = 0; cntb < qntBotoesCientifica; cntb++){
			botoesCientifica[cntb].style.width = "5%";
		}
	}
}
function backspace(){
    var cnt_tela = document.getElementById("calc-display").innerHTML;
    var cnt_tela_tmp;
    if(cnt_tela !== "0"){
        if(cnt_tela.length === 1){
            document.getElementById("calc-display").innerHTML = "0";
        }else{
            cnt_tela_tmp = cnt_tela[0];
            for(var i = 1; i < cnt_tela.length -1; i++){
                cnt_tela_tmp = cnt_tela_tmp + cnt_tela[i];
            }
            document.getElementById("calc-display").innerHTML = cnt_tela_tmp;
        }
    }
}
function debugVar(){
    console.log("Valor de x: "+x+", valor do operador: "+ope+", valor do x tratado: "+xValor+", valor do y tratado: "+yValor+", valor do resultTela: "+resultTela+".");
}
