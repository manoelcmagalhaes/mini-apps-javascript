document.addEventListener('keydown', function (event) {
    if (event.keyCode === 96) {
        x = 0;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 97) {
        x = 1;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 98) {
        x = 2;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 99) {
        x = 3;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 100) {
        x = 4;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 101) {
        x = 5;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 102) {
        x = 6;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 103) {
        x = 7;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 104) {
        x = 8;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 105) {
        x = 9;
        inicio();
        document.getElementById("botao_"+x).disabled = true;
    } else if (event.keyCode === 110) {
        x = ",";
        mudaVirgula();
        document.getElementById("botao_ponto").disabled = true;
    } else if (event.keyCode === 111) {
        ope = "/";
        limpaTela();
        document.getElementById("botao_divisao").disabled = true;
    } else if (event.keyCode === 106) {
        ope = "*";
        limpaTela();
        document.getElementById("botao_multiplicacao").disabled = true;
    } else if (event.keyCode === 109) {
        ope = "-";
        limpaTela();
        document.getElementById("botao_subtracao").disabled = true;
    } else if (event.keyCode === 107) {
        ope = "+";
        limpaTela();
        document.getElementById("botao_soma").disabled = true;
    } else if (event.keyCode === 111) {
        ope = "/";
        limpaTela();
        document.getElementById("botao_divisao").disabled = true;
    } else if (event.keyCode === 194) {
        ope = ",";
        limpaTela();
        document.getElementById("botao_ponto").disabled = true;
    } else if (event.keyCode === 13) {
        eq = "=";
        igualPressionado();
        document.getElementById("botao_igual").disabled = true;
    } else if (event.keyCode === 27) {
        limpaTudo();
        document.getElementById("botao_limpar").disabled = true;
    } else if (event.keyCode === 8) {
        backspace();
    }
});
document.addEventListener('keyup', function (event) {
    if (event.keyCode === 96) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 97) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 98) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 99) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 100) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 101) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 102) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 103) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 104) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 105) {
        document.getElementById("botao_"+x).disabled = false;
    } else if (event.keyCode === 110) {
        document.getElementById("botao_ponto").disabled = false;
    } else if (event.keyCode === 111) {
        document.getElementById("botao_divisao").disabled = false;
    } else if (event.keyCode === 106) {
        document.getElementById("botao_multiplicacao").disabled = false;
    } else if (event.keyCode === 109) {
        document.getElementById("botao_subtracao").disabled = false;
    } else if (event.keyCode === 107) {
        document.getElementById("botao_soma").disabled = false;
    } else if (event.keyCode === 111) {
        document.getElementById("botao_divisao").disabled = false;
    } else if (event.keyCode === 194) {
        document.getElementById("botao_ponto").disabled = false;
    } else if (event.keyCode === 13) {
        document.getElementById("botao_igual").disabled = false;
    } else if (event.keyCode === 27) {
        document.getElementById("botao_limpar").disabled = false;
    }
});