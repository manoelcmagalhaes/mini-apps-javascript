function gamearray(){
    var nomearray = document.getElementById("campo").value;
    var entradasarray = document.getElementById("entradas").value.split(" ");
    var arraytipo = parseInt(document.getElementById("tipo").value);
    var arraysaida = "";
    if(arraytipo === 1){
        for(var i = 0; i < entradasarray.length; i++){
            if(i === 0){
                arraysaida = arraysaida + "[\"" + entradasarray[i] + "\"";
            } else if(i !== entradasarray.length -1 && i > 0){
                arraysaida = arraysaida + ", \"" + entradasarray[i] + "\"";
            } else if(i === entradasarray.length -1){
                arraysaida = arraysaida + ", \"" + entradasarray[i] + "\"]";
            }
        }
    } else if (arraytipo === 2){
        for(var i = 0; i < entradasarray.length; i++){
            if(i === 0){
                arraysaida = arraysaida + "[" + entradasarray[i] + "";
            } else if(i !== entradasarray.length -1 && i > 0){
                arraysaida = arraysaida + ", " + entradasarray[i] + "";
            } else if(i === entradasarray.length -1){
                arraysaida = arraysaida + ", " + entradasarray[i] + "]";
            }
        }
    }
    document.getElementById("resultado").innerHTML = "var " + nomearray + " = " + arraysaida + ";";
}