document.addEventListener("load", funcesp());
function piramide() {
    var entrada = document.getElementById("entrada").value;
    var tmp, tmpflo, outrolado;
    if (parseFloat(entrada) > 10 || entrada === 0) {
        alert("Só funciona com valores entre 1 e 10");
    } else if (entrada === "") {
        alert("Insira um valor entre 1 e 10");
    } else {
        document.getElementById("areapiramide").innerHTML = "<div><br></div>";
        for (var i = 1; i < entrada; i++) {
            if (i === 1) {
                tmp = "1";
            } else {
                tmp = tmp + i;
            }
            tmpflo = parseFloat(tmp);
            outrolado = (tmpflo * 8) + i;
            document.getElementById("areapiramide").innerHTML += "<p class='text-center' id=" + i + ">" + tmp + " x 8 + " + i + " = " + outrolado + "</p>";
        }
    }
}
function teste(loop) {
    var contagem = 0;
    for (var i = 0; i <= loop; i++) {
        var teste = loop / (loop - i);
        if (Number.isInteger(teste)) {
            contagem++;
        }
        if (i === loop) {
            if (contagem === 2) {
                //document.getElementById("listaprimos").innerHTML += "<p class='text-center' id=" + i + ">" + loop + " é primo</p>";
                console.log(loop + " é primo");
            }
        }
    }
}
function primos() {
    document.getElementById("listaprimos").innerHTML = "<div><br></div>";
    var loop = parseFloat(document.getElementById("entrada").value);
    for (var i = 0; i < loop; i++) {
        teste(loop - i);
    }
}
function inverter() {
    var loop = document.getElementById("entrada").value;
    var ordenado = "";
    for (var i = loop.length - 1; i >= 0; i--) {
        ordenado = ordenado + loop[i];
        console.log("O valor do contador é:" + i);
        console.log("O valor de ordenado é:" + ordenado);
        console.log("O valor de loop é: " + loop[i]);
    }
    console.log(ordenado);
}
function desenha() {
    var svgns = "http://www.w3.org/2000/svg";
    var svg = document.getElementById('svg');
    var shape = document.createElementNS(svgns, "circle");
    shape.setAttributeNS(null, "cx", 25);
    shape.setAttributeNS(null, "cy", 25);
    shape.setAttributeNS(null, "r", 50);
    shape.setAttributeNS(null, "fill", "pink");
    svg.appendChild(shape);
}
function triangula() {
    var v1a = document.getElementById('valor1a').value;
    var v1b = document.getElementById('valor1b').value;
    var v2a = document.getElementById('valor2a').value;
    var v2b = document.getElementById('valor2b').value;
    var v3a = document.getElementById('valor3a').value;
    var v3b = document.getElementById('valor3b').value;
    var tri_srt = v1a + " " + v1b + ", " + " " + v2a + " " + v2b + ", " + " " + v3a + " " + v3b;
    //document.getElementById('triangulo').setAttribute('points', '100, 0 50, 0 100, 100');
    document.getElementById("triangulo").setAttribute("points", tri_srt);
    console.log(tri_srt);
}
function gerapoligono() {
    document.getElementById("areapoligono").innerHTML += "<div></div>";
    var view_x = parseInt(document.getElementById("view_x").value);
    var view_y = parseInt(document.getElementById("view_y").value);
    var viewbox_size = "0 0 " + view_x + " " + view_y + "";
    document.getElementById("viewbox").setAttribute("viewBox", viewbox_size);
    var angulos = parseInt(document.getElementById('angulos').value);
    var coordenadas = [];
    var pontos = "";
    for (var i = 0; i < angulos; i++) {
        var coordenada_x_if = (Math.floor(Math.random() * (view_x-(view_x/10)) + 1));
        var coordenada_y_if = (Math.floor(Math.random() * (view_y-(view_y/10)) + 1));
        if (i !== (angulos - 1)) {
            coordenadas[i] = "" + coordenada_x_if + " " + coordenada_y_if + ", ";
        } else {
            coordenadas[i] = "" + coordenada_x_if + " " + coordenada_x_if + "";
        }
        var tag = "<div class=\"input-group mb-3\">\
                        <span class=\"input-group-text\" id=\"caixa-coordenada-x-" + i + "\">X-" + (i + 1) + "</span>\
                        <input type='text' id=\"set_cord_x_" + i + "\" value=\"" + coordenada_x_if + "\" class=\"form-control parametros_x\">\
                    </div>\
                    <div class=\"input-group mb-3\">\
                        <span class=\"input-group-text\" id=\"caixa-coordenada-y-" + i + "\">Y-" + (i + 1) + "</span>\
                        <input type=\"text\" id=\"set_cord_y_" + i + "\" value=\"" + coordenada_y_if + "\" class=\"form-control parametros_y\">\
                    </div>\
                    ";
        var tag_final = "<div class=\"input-group mb-3\">\
                        <span class=\"input-group-text\" id=\"caixa-coordenada-x-" + i + "\">X-" + (i + 1) + "</span>\
                        <input type='text' id=\"set_cord_x_" + i + "\" value=\"" + coordenada_x_if + "\" class=\"form-control parametros_x\">\
                    </div>\
                    <div class=\"input-group mb-3\">\
                        <span class=\"input-group-text\" id=\"caixa-coordenada-y-" + i + "\">Y-" + (i + 1) + "</span>\
                        <input type=\"text\" id=\"set_cord_y_" + i + "\" value=\"" + coordenada_y_if + "\" class=\"form-control parametros_y\">\
                        <input type=\"button\" onclick=\"setpontos()\" class=\"btn btn-outline-secondary\" value=\"Inserir\">\
                    </div>\
                    ";
        if (i !== (angulos - 1)) {
            document.getElementById("areapoligono").innerHTML += tag;
        } else {
            document.getElementById("areapoligono").innerHTML += tag_final;
        }
        pontos = pontos + coordenadas[i];
    }
    document.getElementById("poligono").setAttribute("points", pontos);
    document.getElementById("tagpoligono").innerHTML = "" + pontos + "";
    console.log(coordenadas);
    console.log(pontos);
}
function setpontos() {
    var tagpronta = document.getElementById("tag_pronta").value;
    var eixo_x = "";
    var eixo_x_a = [];
    var eixo_y = "";
    var eixo_y_a = [];
    if (tagpronta === "") {
        var input_x = document.getElementsByClassName("parametros_x");
        var input_y = document.getElementsByClassName("parametros_y");
        //var alteracao = "";
        var alteracao = "";
        for (var i = 0; i < input_x.length; i++) {
            if (i !== (input_x.length - 1)) {
                alteracao = alteracao + input_x[i].value + " " + input_y[i].value + ", ";
                eixo_x = eixo_x + input_x[i].value + ", ";
                eixo_y = eixo_y + input_y[i].value + ", ";
            } else {
                alteracao = alteracao + input_x[i].value + " " + input_y[i].value;
                eixo_x = eixo_x + input_x[i].value;
                eixo_y = eixo_y + input_y[i].value;
            }
        }
        document.getElementById("poligono").setAttribute("points", alteracao);
        document.getElementById("tagpoligono").innerHTML = "" + alteracao + "";
        document.getElementById("tagpoligono").innerHTML += "<br> Eixo x: " + eixo_x;
        document.getElementById("tagpoligono").innerHTML += "<br> Eixo y: " + eixo_y;
        console.log(alteracao);
    } else {
        var contador_x = 0;
        var contador_y = 0;
        var pontos_2 = "";
        var pontos_3 = tagpronta.split(",");
        for (var i = 0; i < pontos_3.length; i++) {
            pontos_2 = pontos_2 + pontos_3[i];
        }
        var pontos_1 = pontos_2.split(" ");
        for (var i = 0; i < pontos_1.length; i++) {
            if (i % 2 === 0) {
                eixo_x_a[contador_x] = parseInt(pontos_1[i]);
                if (i !== (pontos_1.length - 2)) {
                    eixo_x = eixo_x + eixo_x_a[contador_x] + ", ";
                } else {
                    eixo_x = eixo_x + eixo_x_a[contador_x];
                }
                contador_x++;
            } else {
                eixo_y_a[contador_y] = parseInt(pontos_1[i]);
                if (i !== (pontos_1.length - 1)) {
                    eixo_y = eixo_y + eixo_y_a[contador_y] + ", ";
                } else {
                    eixo_y = eixo_y + eixo_y_a[contador_y];
                }
                contador_y++;
            }
        }
        var view_x = parseInt(document.getElementById("view_x").value);
        var view_y = parseInt(document.getElementById("view_y").value);
        var viewbox_size = "0 0 " + view_x + " " + view_y + "";
        document.getElementById("viewbox").setAttribute("viewBox", viewbox_size);
        document.getElementById("poligono").setAttribute("points", tagpronta);
        document.getElementById("tagpoligono").innerHTML += "<br> Eixo x: " + eixo_x;
        document.getElementById("tagpoligono").innerHTML += "<br> Eixo y: " + eixo_y;
    }
}
function funcesp() {
    var titulo = document.getElementById("tituloNavbar").innerHTML.split(" ");
    if (titulo[0] === "Poligono") {
        var pontos = document.getElementById("poligono").getAttribute("points");
        document.getElementById("tagpoligono").innerHTML = pontos;
    }
    if (titulo[0] === "Gerador") {
        var padrao = document.getElementById("comando").value;
        if (padrao === "1") {
            document.getElementById("fpad").style.display = "flex";
            document.getElementById("gpad").style.display = "none";
            document.getElementById("rotulo_campo").innerHTML = "Campo:";
            document.getElementById("campo").placeholder = "Campo:";
            document.getElementById("div_cor").style.display = "flex";
            document.getElementById("div_cursos").style.display = "flex";
            document.getElementById("div_con").style.display = "none";
            document.getElementById("div_valor").style.display = "none";
            document.getElementById("rotulo_final_cur").style.display = "none";
            document.getElementById("cur_final").style.display = "none";
            document.getElementById("resultado").innerHTML = "";
        } else if (padrao === "2") {
            document.getElementById("fpad").style.display = "flex";
            document.getElementById("gpad").style.display = "none";
            document.getElementById("rotulo_campo").innerHTML = "Campo:";
            document.getElementById("campo").placeholder = "Campo:";
            document.getElementById("div_con").style.display = "flex";
            document.getElementById("div_valor").style.display = "flex";
            document.getElementById("div_cor").style.display = "none";
            document.getElementById("div_cursos").style.display = "none";
            document.getElementById("rotulo_final_cur").style.display = "flex";
            document.getElementById("cur_final").style.display = "flex";
            document.getElementById("resultado").innerHTML = "";
        } else if (padrao === "3") {
            document.getElementById("fpad").style.display = "flex";
            document.getElementById("gpad").style.display = "none";
            document.getElementById("rotulo_campo").innerHTML = "Célula:";
            document.getElementById("campo").placeholder = "Célula:";
            document.getElementById("div_con").style.display = "flex";
            document.getElementById("div_valor").style.display = "flex";
            document.getElementById("div_cor").style.display = "none";
            document.getElementById("div_cursos").style.display = "none";
            document.getElementById("resultado").innerHTML = "";
        } else if (padrao === "4") {
            document.getElementById("fpad").style.display = "none";
            document.getElementById("gpad").style.display = "flex";
            document.getElementById("rotulo_campo").innerHTML = "Array:";
            document.getElementById("campo").placeholder = "Nome do array";
        }
    }
}
function coresreport() {
    var validacao = "";
    var padrao = document.getElementById("comando").value;
    if (padrao === "1") {
        var cor_curso = document.getElementById("cor").value;
        var campo_curso = document.getElementById("campo").value;
        var curso = document.getElementById("cursos").value;
        if (cor_curso === "" || campo_curso === "" || curso === "") {
            alert("Preencha os campos vazios!");
        } else {
            validacao = "ok";
        }
    } else if (padrao === "2" || padrao === "3") {
        var difere = false;
        var var_final = "";
        var campo = document.getElementById("campo").value;
        var condi = document.getElementById("condicao").value;
        var ref = document.getElementById("ref").value;
        var valor = document.getElementById("valor").value;
        var ref_a = document.getElementById("ref").value.split(", ");
        var valor_a = document.getElementById("valor").value.split(", ");
        if (padrao === "2") {
            var_final = document.getElementById("cur_final").value;
        } else if (padrao === "3") {
            var_final = document.getElementById("val_final").value;
        }
        if (ref_a.length === 1 || valor_a.length === 1) {
            var_final = "ok";
        }
        if (ref_a.length !== valor_a.length) {
            difere = true;
        }
        if (campo === "" || condi === "" || ref === "" || valor === "" || var_final === "") {
            if (difere === true) {
                alert("Preencha os campos vazios e insira referencia e valores com a mesma quantidade de itens!");
            } else {
                alert("Preencha os campos vazios");
            }
        } else {
            if (difere === true) {
                alert("Os itens da referencia e dos valores devem ter a mesma quantidade, verifique as virgulas");
            } else {
                validacao = "ok";
            }
        }

    }
    if (validacao === "ok") {
        if (padrao === "1") {
            var cor_curso = document.getElementById("cor").value;
            var campo_curso = document.getElementById("campo").value;
            var curso = document.getElementById("cursos").value.split(", ");
            var strpronta = "";
            for (var i = 0; i < curso.length; i++) {
                if (i !== curso.length - 1) {
                    if (i === 0) {
                        strpronta = strpronta + "=Switch(Fields!" + campo_curso + ".Value=" + curso[i] + ", \"" + cor_curso + "\", ";
                    } else {
                        strpronta = strpronta + "Fields!" + campo_curso + ".Value=" + curso[i] + ", \"" + cor_curso + "\", ";
                    }
                } else {
                    strpronta = strpronta + "Fields!" + campo_curso + ".Value=" + curso[i] + ", \"" + cor_curso + "\")";
                }
            }
            document.getElementById("resultado").innerHTML = strpronta;
            console.log(strpronta);
        } else if (padrao === "2") {
            var campo = document.getElementById("campo").value;
            var condi = document.getElementById("condicao").value;
            var ref = document.getElementById("ref").value.split(", ");
            var valor = document.getElementById("valor").value.split(", ");
            var final = document.getElementById("cur_final").value;
            var strpronta = "";
            var fechamento = "";
            for (var i = 0; i < ref.length; i++) {
                fechamento = fechamento + ")";
                if (i !== ref.length - 1) {
                    if (i === 0) {
                        strpronta = strpronta + "=LIF(Fields!" + campo + ".Value " + condi + " " + ref[i] + ", \"" + valor[i] + "\", ";
                    } else {
                        strpronta = strpronta + "LIF(Fields!" + campo + ".Value " + condi + " " + ref[i] + ", \"" + valor[i] + "\", ";
                    }
                } else {
                    if (ref.length === 1) {
                        strpronta = strpronta + "=LIF(Fields!" + campo + ".Value " + condi + " " + ref[i] + ", \"" + valor[i] + "\"" + fechamento;
                    } else {
                        strpronta = strpronta + "LIF(Fields!" + campo + ".Value " + condi + " " + ref[i] + ", \"" + valor[i] + "\", \"" + final + "\"" + fechamento;
                    }
                }
            }
            document.getElementById("resultado").innerHTML = strpronta;
            console.log(strpronta);
        } else if (padrao === "3") {
            var campo = document.getElementById("campo").value;
            var condi = document.getElementById("condicao").value;
            var ref = document.getElementById("ref").value.split(", ");
            var valor = document.getElementById("valor").value.split(", ");
            var final = document.getElementById("val_final").value;
            var strpronta = "";
            var fechamento = "";
            for (var i = 0; i < ref.length; i++) {
                fechamento = fechamento + ")";
                if (i !== ref.length - 1) {
                    if (i === 0) {
                        strpronta = strpronta + "=SE(" + campo + " " + condi + " " + ref[i] + "; \"" + valor[i] + "\"; ";
                    } else {
                        strpronta = strpronta + "SE(" + campo + " " + condi + " " + ref[i] + "; \"" + valor[i] + "\"; ";
                    }
                } else {
                    if (ref.length === 1) {
                        strpronta = strpronta + "=SE(" + campo + " " + condi + " " + ref[i] + "; \"" + valor[i] + "\"" + fechamento;
                    } else {
                        strpronta = strpronta + "SE(" + campo + " " + condi + " " + ref[i] + "; \"" + valor[i] + "\"; \"" + final + "\"" + fechamento;
                    }
                }
            }
            document.getElementById("resultado").innerHTML = strpronta;
            console.log(strpronta);
        }
    }
}
function idcoord() {
    var pontos = document.getElementById("poligono").getAttribute("points").split(", ");
    var viewsize = document.getElementById("viewbox").getAttribute("viewBox").split(" ");
    var fontsize = ((parseInt(viewsize[2]))+(parseInt(viewsize[3]))/2)/20;
    var listapontos = document.getElementsByClassName("txtcoord");
    for(var h = 0; h < listapontos; h++){
        listapontos[h].remove();
    }
    for (var i = 0; i < pontos.length; i++) {
        var pontos_div = pontos[i].split(" ");
        var x = pontos_div[0];
        var y = pontos_div[1];
        var tag = "<text x=\"" + x + "\" y=\"" + y + "\" fill=\"red\" font-size=\""+fontsize+"px\" class=\"txtcoord\">x:" + x + ", y:" + y + "</text>";
        document.getElementById("viewbox").innerHTML += tag;
        console.log(tag);
    }
}

/*ID int auto_increment PRIMARY KEY,*/

function gera(){
	let schema = document.getElementById("entrada").value.split(" ");
	let tabela = document.getElementById("tabela").value;
	let arraySchema = [];
	let codigoSql = "";
	for(let i = 0; i < schema.length; i++){
		arraySchema[i] = schema[i].split("\t");
	}
	for(let l = 0; l < arraySchema.length; l++){
		if(arraySchema[l][3] === "NO"){
			arraySchema[l][3] = "not null";
		}else if(arraySchema[l][3] === "YES"){
			arraySchema[l][3] = "null";
		}
		let tamanhoTmp = arraySchema[l][2];
		if(arraySchema[l][1] === "text" || arraySchema[l][1] === "image"){
			arraySchema[l][2] = "";
			if(arraySchema[l][1] === "image"){
				arraySchema[l][1] = "text";
			}
		}else{
			if(tamanhoTmp !== "NULL"){
				arraySchema[l][2] = "("+tamanhoTmp+")";
			}else{
				arraySchema[l][2] = "";
			}
		}
	}
	console.log(arraySchema[0][2]);
	for(let k = 0; k < arraySchema.length; k++){
		if(k === 0){
			codigoSql += "create table "+tabela+"(\n\t"+arraySchema[k][0]+" "+arraySchema[k][1]+""+arraySchema[k][2]+" "+arraySchema[k][3]+",\n";
		}else if(k > 0 && k < arraySchema.length -2){
			codigoSql += "\t"+arraySchema[k][0]+" "+arraySchema[k][1]+""+arraySchema[k][2]+" "+arraySchema[k][3]+",\n";
		}else if(k === arraySchema.length -2){
			codigoSql += "\t"+arraySchema[k][0]+" "+arraySchema[k][1]+""+arraySchema[k][2]+" "+arraySchema[k][3]+",\n";
		}else if(k === arraySchema.length -1){
			codigoSql += "\t"+arraySchema[k][0]+" "+arraySchema[k][1]+""+arraySchema[k][2]+" "+arraySchema[k][3]+"\n);";
		}
	}
	console.log(codigoSql);
	document.getElementById("conversao").innerHTML = codigoSql;
}
