var eixo_x = [];
var eixo_y = [];
var direcionamento = "cima";
var direcao = "";
var passo = 0;
function identificapontos(debug) {
    var contador_x = 0;
    var contador_y = 0;
    var pontos_2 = "";
    var pontos_3 = document.getElementById("tanque").getAttribute("points").split(",");
    for (var i = 0; i < pontos_3.length; i++) {
        pontos_2 = pontos_2 + pontos_3[i];
    }
    var pontos_1 = pontos_2.split(" ");
    for (var i = 0; i < pontos_1.length; i++) {
        if (i % 2 === 0) {
            eixo_x[contador_x] = parseInt(pontos_1[i]);
            contador_x++;
        } else {
            eixo_y[contador_y] = parseInt(pontos_1[i]);
            contador_y++;
        }
    }
    if(debug === true){
        console.log(eixo_x);
        console.log(eixo_y);
    }
}
document.addEventListener('keydown', function (event) {
    if (event.keyCode === 40) {
        movetanque("baixo", 1);
        direcionamento = "baixo";
    } else if (event.keyCode === 38) {
        movetanque("cima", 1);
        direcionamento = "cima";
    } else if (event.keyCode === 37) {
        movetanque("esquerda", 1);
        direcionamento = "esquerda";
    } else if (event.keyCode === 39) {
        movetanque("direita", 1);
        direcionamento = "direita";
    }
});
function movetanque(dire, velocidade) {
    direcao = dire;
    var instrucao = "";
    if (direcao === "baixo") {
        gira();
        for (var i = 0; i < eixo_y.length; i++) {
            eixo_y[i] = eixo_y[i] + velocidade;
            if (i !== eixo_y.length - 1) {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i] + ", ";
            } else {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i];
            }
        }
        document.getElementById("tanque").setAttribute("points", instrucao);
    } else if (direcao === "cima") {
        gira();
        for (var i = 0; i < eixo_y.length; i++) {
            eixo_y[i] = eixo_y[i] - velocidade;
            if (i !== eixo_y.length - 1) {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i] + ", ";
            } else {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i];
            }
        }
        document.getElementById("tanque").setAttribute("points", instrucao);
    } else if (direcao === "direita") {
        gira();
        for (var i = 0; i < eixo_y.length; i++) {
            eixo_x[i] = eixo_x[i] + velocidade;
            if (i !== eixo_x.length - 1) {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i] + ", ";
            } else {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i];
            }
        }
        document.getElementById("tanque").setAttribute("points", instrucao);
    } else if (direcao === "esquerda") {
        gira();
        for (var i = 0; i < eixo_y.length; i++) {
            eixo_x[i] = eixo_x[i] - velocidade;
            if (i !== eixo_x.length - 1) {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i] + ", ";
            } else {
                instrucao = instrucao + eixo_x[i] + " " + eixo_y[i];
            }
        }
        document.getElementById("tanque").setAttribute("points", instrucao);
    }
}
function rotaciona(sentido, vezes) {
    var contador = 0;
    identificapontos();
    for (var v = 1; v <= vezes; v++) {
        console.log(contador);
        var novo_eixo_x = [];
        var novo_eixo_y = [];
        var rotaciona = "";
        var soma_x = 0;
        var soma_y = 0;
        if (sentido === "hora") {
            var sen = 1;
            var cos = 0;
            soma_x = 15;
        } else if (sentido === "anti") {
            var sen = -1;
            var cos = 0;
            soma_y = 15;
        } else {
            console.log("nada");
        }
        if (sentido === "anti" || sentido === "hora") {
            for (var i = 0; i < eixo_x.length; i++) {
                novo_eixo_x[i] = (eixo_x[i] * cos - eixo_y[i] * sen) + soma_x;
                novo_eixo_y[i] = (eixo_x[i] * sen + eixo_y[i] * cos) + soma_y;
                eixo_x[i] = novo_eixo_x[i];
                eixo_y[i] = novo_eixo_y[i];
            }
            for (var i = 0; i < novo_eixo_x.length; i++) {
                if (i !== novo_eixo_x.length - 1) {
                    rotaciona = rotaciona + novo_eixo_x[i] + " " + novo_eixo_y[i] + ", ";
                } else {
                    rotaciona = rotaciona + novo_eixo_x[i] + " " + novo_eixo_y[i];
                }
            }
        }
        document.getElementById("tanque").setAttribute("points", rotaciona);
        contador++;
    }
}
function gira() {
    if (direcionamento === "cima") {
        if (direcao === "baixo") {
            rotaciona("anti", 2);
            calculapassos();
        } else if (direcao === "direita") {
            rotaciona("hora", 1);
            calculapassos();
        } else if (direcao === "esquerda") {
            rotaciona("anti", 1);
            calculapassos();
        } else if (direcao === "cima") {
            passo++;
        }
    } else if (direcionamento === "baixo") {
        if (direcao === "direita") {
            rotaciona("anti", 1);
            calculapassos();
        } else if (direcao === "esquerda") {
            rotaciona("hora", 1);
            calculapassos();
        } else if (direcao === "cima") {
            rotaciona("anti", 2);
            calculapassos();
        } else if (direcao === "baixo") {
            passo++;
        }
    } else if (direcionamento === "esquerda") {
        if (direcao === "baixo") {
            rotaciona("anti", 1);
            calculapassos();
        } else if (direcao === "direita") {
            rotaciona("anti", 2);
            calculapassos();
        } else if (direcao === "cima") {
            rotaciona("hora", 1);
            calculapassos();
        } else if (direcao === "esquerda") {
            passo++;
        }
    } else if (direcionamento === "direita") {
        if (direcao === "baixo") {
            rotaciona("hora", 1);
            calculapassos();
        } else if (direcao === "esquerda") {
            rotaciona("anti", 2);
            calculapassos();
        } else if (direcao === "cima") {
            rotaciona("anti", 1);
            calculapassos();
        } else if (direcao === "direita") {
            passo++;
        }
    }
    identificapontos();
}
function calculapassos(){
    identificapontos();
    var soma_x = 0;
    var soma_y = 0;
    for(var i = 0; i < eixo_x.length; i++){
        var soma_x = soma_x + eixo_x[i];
        console.log(soma_x);
        var soma_y = soma_y + eixo_y[i];
        console.log(soma_y);
    }
    if(soma_x > soma_y){
        for(var i = 0; i < eixo_x.length; i++){
            eixo_x[i] = eixo_x[i] - passo;
            console.log(eixo_x[i]);
            eixo_y[i] = eixo_y[i] + passo;
            console.log(eixo_y[i]);
        }
    } else if(soma_x < soma_y){
      for(var i = 0; i < eixo_x.length; i++){
          eixo_y[i] = eixo_y[i] - passo;
          console.log(eixo_y[i]);
          eixo_x[i] = eixo_x[i] + passo;
          console.log(eixo_x[i]);
      }
    }
    passo = 0;
}
