var eixo_x = []; //Cria o array que vai armazenar todos os pontos x da figura atual
var novo_eixo_x = []; //Cria o array que vai armazenar todos os pontos x temporarios que serão utilizados para modificação da figura atual
var eixo_y = []; //Cria o array que vai armazenar todos os pontos y da figura atual
var novo_eixo_y = []; //Cria o array que vai armazenar todos os pontos y temporarios que serão utilizados para modificação da figura atual
var direcionamento = "cima"; //Variavel que armazena o posicionamento atual da figura, já começa definida para cima

/*
 * Função anonima responsavel por receber e processar as entradas do teclado
 */

document.addEventListener('keydown', function (event) {
    if (event.keyCode === 40) {
        movimento("baixo", 1); //Para baixo
    } else if (event.keyCode === 38) {
        movimento("cima", 1); //Para cima
    } else if (event.keyCode === 37) {
        movimento("esquerda", 1); //Para esquerda
    } else if (event.keyCode === 39) {
        movimento("direita", 1); //Para direita
    }
});

/*
 * Função de movimento, ela verifica o posicionamento atual da figura e em seguida chama
 * as funções de rotação e translação da figura
 */

function movimento(direcao, velocidade) {
    if (direcao !== direcionamento) {
        if (direcionamento === "cima") {
            rota_cima(direcao);
        } else if(direcionamento === "baixo") {
            rota_baixo(direcao);
        } else if(direcionamento === "esquerda") {
            rota_esquerda(direcao);
        } else if(direcionamento === "direita") {
            rota_direita(direcao);
        }
    }
    movetanque(direcao, velocidade);
    direcionamento = direcao;
}

/*
 * Função responsavel por identificar ou definir os pontos da figura atual
 */

function identificapontos(funcao) {
    if (funcao === "captura") {
        var contador_x = 0;
        var contador_y = 0;
        var pontos_2 = "";
        var pontos_3 = document.getElementById("tanque").getAttribute("points").split(",");
        for (var i = 0; i < pontos_3.length; i++) {
            pontos_2 = pontos_2 + pontos_3[i];
        }
        var pontos_1 = pontos_2.split(" ");
        for (var i = 0; i < pontos_1.length; i++) {
            if (i % 2 === 0) {
                eixo_x[contador_x] = parseInt(pontos_1[i]);
                contador_x++;
            } else {
                eixo_y[contador_y] = parseInt(pontos_1[i]);
                contador_y++;
            }
        }
    } else if (funcao === "set") {
        var pontos = "";
        for (var i = 0; i < eixo_x.length; i++) {
            if (i !== eixo_x.length - 1) {
                pontos = pontos + novo_eixo_x[i] + " " + novo_eixo_y[i] + ", ";
            } else if (i === eixo_x.length - 1) {
                pontos = pontos + novo_eixo_x[i] + " " + novo_eixo_y[i];
            }
        }
        document.getElementById("tanque").setAttribute("points", pontos);
    }
}

/*
 * Função que realiza a translação horizontal ou vertical da figura
 */

function movetanque(direcao, velocidade) {
    identificapontos("captura");
    if (direcao === "esquerda") {
        for (var i = 0; i < eixo_x.length; i++) {
            novo_eixo_x[i] = eixo_x[i] - velocidade;
            novo_eixo_y[i] = eixo_y[i];
        }
    } else if (direcao === "direita") {
        for (var i = 0; i < eixo_x.length; i++) {
            novo_eixo_x[i] = eixo_x[i] + velocidade;
            novo_eixo_y[i] = eixo_y[i];
        }
    } else if (direcao === "cima") {
        for (var i = 0; i < eixo_y.length; i++) {
            novo_eixo_y[i] = eixo_y[i] - velocidade;
            novo_eixo_x[i] = eixo_x[i];
        }
    } else if (direcao === "baixo") {
        for (var i = 0; i < eixo_y.length; i++) {
            novo_eixo_y[i] = eixo_y[i] + velocidade;
            novo_eixo_x[i] = eixo_x[i];
        }
    }
    identificapontos("set");
}

//Instruções de rotação
// -- Cima --
// Direita
var parametros_x_cima_direita_op = ["nada", "soma", "soma", "soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai", "nada", "nada", "nada"];
var parametros_y_cima_direita_op = ["subtrai", "subtrai", "soma", "soma", "soma", "soma", "nada", "nada", "nada", "nada", "subtrai", "subtrai"];
var parametros_x_cima_direita_valores = [0, 5, 5, 5, 5, 5, 5, 10, 10, 0, 0, 0];
var parametros_y_cima_direita_valores = [5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 10, 10];
// Esquerda
var parametros_x_cima_esquerda_op = ["nada", "nada", "nada", "soma", "soma", "subtrai", "subtrai", "soma", "soma", "nada", "nada", "nada"];
var parametros_y_cima_esquerda_op = ["nada", "nada", "nada", "nada", "nada", "nada", "subtrai", "subtrai", "soma", "soma", "subtrai", "subtrai"];
var parametros_x_cima_esquerda_valores = [0, 0, 0, 5, 5, 5, 5, 5, 5, 0, 0, 0];
var parametros_y_cima_esquerda_valores = [0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5];
// Baixo
var parametros_x_cima_baixo_op = ["nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada"];
var parametros_y_cima_baixo_op = ["subtrai", "subtrai", "soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai", "soma", "soma", "subtrai", "subtrai"];
var parametros_x_cima_baixo_valores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var parametros_y_cima_baixo_valores = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
// -- Baixo --
// Direita
var parametros_x_baixo_direita_op = ["nada", "soma", "soma", "soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai", "nada", "nada", "nada"];
var parametros_y_baixo_direita_op = ["nada", "nada", "nada", "nada", "soma", "soma", "soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai"];
var parametros_x_baixo_direita_valores = [0, 5, 5, 5, 5, 5, 5, 10, 10, 0, 0, 0];
var parametros_y_baixo_direita_valores = [0, 0, 0, 0, 10, 10, 5, 5, 5, 5, 5, 5];
// Esquerda
var parametros_x_baixo_esquerda_op = ["nada", "nada", "nada", "soma", "soma", "subtrai", "subtrai", "soma", "soma", "nada", "nada", "nada"];
var parametros_y_baixo_esquerda_op = ["soma", "soma", "subtrai", "subtrai", "soma", "soma", "nada", "nada", "nada", "nada", "nada", "nada"];
var parametros_x_baixo_esquerda_valores = [0, 0, 0, 5, 5, 5, 5, 5, 5, 0, 0, 0];
var parametros_y_baixo_esquerda_valores = [5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0];
// Cima
var parametros_x_baixo_cima_op = ["nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada", "nada"];
var parametros_y_baixo_cima_op = ["soma", "soma", "subtrai", "subtrai", "soma", "soma", "soma", "soma", "subtrai", "subtrai", "soma", "soma"];
var parametros_x_baixo_cima_valores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var parametros_y_baixo_cima_valores = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
// -- Direita --
// Cima
var parametros_x_direita_cima_op = ["nada", "subtrai", "subtrai", "subtrai", "subtrai", "soma", "soma", "soma", "soma", "nada", "nada", "nada"];
var parametros_y_direita_cima_op = ["soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai", "nada", "nada", "nada", "nada", "soma", "soma"];
var parametros_x_direita_cima_valores = [0, 5, 5, 5, 5, 5, 5, 10, 10, 0, 0, 0];
var parametros_y_direita_cima_valores = [5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 10, 10];
// Baixo
var parametros_x_direita_baixo_op = ["nada", "subtrai", "subtrai", "subtrai", "subtrai", "soma", "soma", "soma", "soma", "nada", "nada", "nada"];
var parametros_y_direita_baixo_op = ["nada", "nada", "nada", "nada", "subtrai", "subtrai", "subtrai", "subtrai", "soma", "soma", "soma", "soma"];
var parametros_x_direita_baixo_valores = [0, 5, 5, 5, 5, 5, 5, 10, 10, 0, 0, 0];
var parametros_y_direita_baixo_valores = [0, 0, 0, 0, 10, 10, 5, 5, 5, 5, 5, 5];
// Esquerda
var parametros_x_direita_esquerda_op = ["nada", "subtrai", "subtrai", "nada", "nada", "nada", "nada", "soma", "soma", "nada", "nada", "nada"];
var parametros_y_direita_esquerda_op = ["soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai", "subtrai", "subtrai", "soma", "soma", "soma", "soma"];
var parametros_x_direita_esquerda_valores = [0, 5, 5, 0, 0, 0, 0, 15, 15, 0, 0, 0];
var parametros_y_direita_esquerda_valores = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
// -- Esquerda --
// Cima
var parametros_x_esquerda_cima_op = ["nada", "nada", "nada", "subtrai", "subtrai", "soma", "soma", "subtrai", "subtrai", "nada", "nada", "nada"];
var parametros_y_esquerda_cima_op = ["nada", "nada", "nada", "nada", "nada", "nada", "soma", "soma", "subtrai", "subtrai", "soma", "soma"];
var parametros_x_esquerda_cima_valores = [0, 0, 0, 5, 5, 5, 5, 5, 5, 0, 0, 0];
var parametros_y_esquerda_cima_valores = [0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, 5];
// Baixo
var parametros_x_esquerda_baixo_op = ["nada", "nada", "nada", "subtrai", "subtrai", "soma", "soma", "subtrai", "subtrai", "nada", "nada", "nada"];
var parametros_y_esquerda_baixo_op = ["subtrai", "subtrai", "soma", "soma", "subtrai", "subtrai", "nada", "nada", "nada", "nada", "nada", "nada"];
var parametros_x_esquerda_baixo_valores = [0, 0, 0, 5, 5, 5, 5, 5, 5, 0, 0, 0];
var parametros_y_esquerda_baixo_valores = [5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0];
// Direita
var parametros_x_esquerda_direita_op = ["nada", "soma", "soma", "nada", "nada", "nada", "nada", "subtrai", "subtrai", "nada", "nada", "nada"];
var parametros_y_esquerda_direita_op = ["subtrai", "subtrai", "soma", "soma", "soma", "soma", "soma", "soma", "subtrai", "subtrai", "subtrai", "subtrai"];
var parametros_x_esquerda_direita_valores = [0, 5, 5, 0, 0, 0, 0, 15, 15, 0, 0, 0];
var parametros_y_esquerda_direita_valores = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];

/*
 * Bloco das funções de rotação
 * elas realizam a rotação da figura atual a partir dos parametros informados acima
 */

//Rotação a partir da posição inicial "cima"
function rota_cima(direcao) {
    identificapontos("captura");
    if (direcao === "direita") {
        for (var i = 0; i < parametros_x_cima_direita_op.length; i++) {
            if (parametros_x_cima_direita_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_cima_direita_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_cima_direita_valores[i];
            } else if (parametros_x_cima_direita_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_cima_direita_valores[i];
            }
            if (parametros_y_cima_direita_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_cima_direita_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_cima_direita_valores[i];
            } else if (parametros_y_cima_direita_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_cima_direita_valores[i];
            }
        }
    } else if (direcao === "esquerda") {
        for (var i = 0; i < parametros_x_cima_esquerda_op.length; i++) {
            if (parametros_x_cima_esquerda_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_cima_esquerda_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_cima_esquerda_valores[i];
            } else if (parametros_x_cima_esquerda_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_cima_esquerda_valores[i];
            }
            if (parametros_y_cima_esquerda_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_cima_esquerda_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_cima_esquerda_valores[i];
            } else if (parametros_y_cima_esquerda_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_cima_esquerda_valores[i];
            }
        }
    } else if (direcao === "baixo") {
        for (var i = 0; i < parametros_x_cima_baixo_op.length; i++) {
            if (parametros_x_cima_baixo_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_cima_baixo_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_cima_baixo_valores[i];
            } else if (parametros_x_cima_baixo_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_cima_baixo_valores[i];
            }
            if (parametros_y_cima_baixo_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_cima_baixo_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_cima_baixo_valores[i];
            } else if (parametros_y_cima_baixo_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_cima_baixo_valores[i];
            }
        }
    }
    identificapontos("set");
}

//Rotação a partir da posição inicial "baixo"
function rota_baixo(direcao) {
    identificapontos("captura");
    if (direcao === "direita") {
        for (var i = 0; i < parametros_x_baixo_direita_op.length; i++) {
            if (parametros_x_baixo_direita_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_baixo_direita_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_baixo_direita_valores[i];
            } else if (parametros_x_baixo_direita_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_baixo_direita_valores[i];
            }
            if (parametros_y_baixo_direita_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_baixo_direita_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_baixo_direita_valores[i];
            } else if (parametros_y_baixo_direita_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_baixo_direita_valores[i];
            }
        }
    } else if (direcao === "esquerda") {
        for (var i = 0; i < parametros_x_baixo_esquerda_op.length; i++) {
            if (parametros_x_baixo_esquerda_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_baixo_esquerda_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_baixo_esquerda_valores[i];
            } else if (parametros_x_baixo_esquerda_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_baixo_esquerda_valores[i];
            }
            if (parametros_y_baixo_esquerda_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_baixo_esquerda_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_baixo_esquerda_valores[i];
            } else if (parametros_y_baixo_esquerda_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_baixo_esquerda_valores[i];
            }
        }
    } else if (direcao === "cima") {
        for (var i = 0; i < parametros_x_baixo_cima_op.length; i++) {
            if (parametros_x_baixo_cima_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_baixo_cima_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_baixo_cima_valores[i];
            } else if (parametros_x_baixo_cima_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_baixo_cima_valores[i];
            }
            if (parametros_y_baixo_cima_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_baixo_cima_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_baixo_cima_valores[i];
            } else if (parametros_y_baixo_cima_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_baixo_cima_valores[i];
            }
        }
    }
    identificapontos("set");
}

//Rotação a partir da posição inicial "direita"
function rota_direita(direcao) {
    identificapontos("captura");
    if (direcao === "baixo") {
        for (var i = 0; i < parametros_x_direita_baixo_op.length; i++) {
            if (parametros_x_direita_baixo_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_direita_baixo_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_direita_baixo_valores[i];
            } else if (parametros_x_direita_baixo_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_direita_baixo_valores[i];
            }
            if (parametros_y_direita_baixo_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_direita_baixo_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_direita_baixo_valores[i];
            } else if (parametros_y_direita_baixo_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_direita_baixo_valores[i];
            }
        }
    } else if (direcao === "esquerda") {
        for (var i = 0; i < parametros_x_direita_esquerda_op.length; i++) {
            if (parametros_x_direita_esquerda_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_direita_esquerda_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_direita_esquerda_valores[i];
            } else if (parametros_x_direita_esquerda_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_direita_esquerda_valores[i];
            }
            if (parametros_y_direita_esquerda_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_direita_esquerda_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_direita_esquerda_valores[i];
            } else if (parametros_y_direita_esquerda_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_direita_esquerda_valores[i];
            }
        }
    } else if (direcao === "cima") {
        for (var i = 0; i < parametros_x_direita_cima_op.length; i++) {
            if (parametros_x_direita_cima_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_direita_cima_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_direita_cima_valores[i];
            } else if (parametros_x_direita_cima_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_direita_cima_valores[i];
            }
            if (parametros_y_direita_cima_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_direita_cima_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_direita_cima_valores[i];
            } else if (parametros_y_direita_cima_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_direita_cima_valores[i];
            }
        }
    }
    identificapontos("set");
}

//Rotação a partir da posição inicial "esquerda"
function rota_esquerda(direcao) {
    identificapontos("captura");
    if (direcao === "baixo") {
        for (var i = 0; i < parametros_x_esquerda_baixo_op.length; i++) {
            if (parametros_x_esquerda_baixo_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_esquerda_baixo_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_esquerda_baixo_valores[i];
            } else if (parametros_x_esquerda_baixo_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_esquerda_baixo_valores[i];
            }
            if (parametros_y_esquerda_baixo_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_esquerda_baixo_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_esquerda_baixo_valores[i];
            } else if (parametros_y_esquerda_baixo_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_esquerda_baixo_valores[i];
            }
        }
    } else if (direcao === "direita") {
        for (var i = 0; i < parametros_x_esquerda_direita_op.length; i++) {
            if (parametros_x_esquerda_direita_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_esquerda_direita_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_esquerda_direita_valores[i];
            } else if (parametros_x_esquerda_direita_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_esquerda_direita_valores[i];
            }
            if (parametros_y_esquerda_direita_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_esquerda_direita_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_esquerda_direita_valores[i];
            } else if (parametros_y_esquerda_direita_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_esquerda_direita_valores[i];
            }
        }
    } else if (direcao === "cima") {
        for (var i = 0; i < parametros_x_esquerda_cima_op.length; i++) {
            if (parametros_x_esquerda_cima_op[i] === "nada") {
                novo_eixo_x[i] = eixo_x[i];
            } else if (parametros_x_esquerda_cima_op[i] === "subtrai") {
                novo_eixo_x[i] = eixo_x[i] - parametros_x_esquerda_cima_valores[i];
            } else if (parametros_x_esquerda_cima_op[i] === "soma") {
                novo_eixo_x[i] = eixo_x[i] + parametros_x_esquerda_cima_valores[i];
            }
            if (parametros_y_esquerda_cima_op[i] === "nada") {
                novo_eixo_y[i] = eixo_y[i];
            } else if (parametros_y_esquerda_cima_op[i] === "subtrai") {
                novo_eixo_y[i] = eixo_y[i] - parametros_y_esquerda_cima_valores[i];
            } else if (parametros_y_esquerda_cima_op[i] === "soma") {
                novo_eixo_y[i] = eixo_y[i] + parametros_y_esquerda_cima_valores[i];
            }
        }
    }
    identificapontos("set");
}