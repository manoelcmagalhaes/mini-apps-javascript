//            a   b   c   d
var eixo_x = [10, 20, 20, 10];
var eixo_y = [10, 10, 20, 20];
var bordas = [0, 0, 300, 150];
var ef_bolhas;
var conta = 0;
window.onload = aparencia();
document.addEventListener('keydown', function (event) {
    if (event.keyCode === 40) {
        squaremove("baixo");
    } else if (event.keyCode === 38) {
        squaremove("cima");
    } else if (event.keyCode === 37) {
        squaremove("esquerda");
    } else if (event.keyCode === 39) {
        squaremove("direita");
    }
});
function squaremove(direcao) {
    var velocidade = parseInt(document.getElementById("sq_velocidade").value);
    if (direcao === "baixo") {
        if (eixo_y[2] < 150 && eixo_y[3] < 150) {
            for (var i = 0; i < eixo_y.length; i++) {
                eixo_y[i] = eixo_y[i] + velocidade;
            }
            var squ_srt = eixo_x[0] + " " + eixo_y[0] + ", " + " " + eixo_x[1] + " " + eixo_y[1] + ", " + " " + eixo_x[2] + " " + eixo_y[2] + ", " + " " + eixo_x[3] + " " + eixo_y[3];
            var squ_srt_debug = "[ Ponto A - eixo x: " + eixo_x[0] + ", eixo y: " + eixo_y[0] + "]  [ Ponto B -" + " eixo x: " + eixo_x[1] + ", eixo y: " + eixo_y[1] + "] [ Ponto C -" + " eixo x: " + eixo_x[2] + ", eixo y: " + eixo_y[2] + "] [ Ponto D: " + " eixo x: " + eixo_x[3] + ", eixo y: " + eixo_y[3] + " ]";
            document.getElementById("quadrado").setAttribute("points", squ_srt);
            console.log("debug: " + squ_srt_debug);
        }

    } else if (direcao === "cima") {
        if (eixo_y[0] > 0 && eixo_y[1] > 0) {
            for (var i = 0; i < eixo_y.length; i++) {
                eixo_y[i] = eixo_y[i] - velocidade;
            }
            var squ_srt = eixo_x[0] + " " + eixo_y[0] + ", " + " " + eixo_x[1] + " " + eixo_y[1] + ", " + " " + eixo_x[2] + " " + eixo_y[2] + ", " + " " + eixo_x[3] + " " + eixo_y[3];
            var squ_srt_debug = "[ Ponto A - eixo x: " + eixo_x[0] + ", eixo y: " + eixo_y[0] + "]  [ Ponto B -" + " eixo x: " + eixo_x[1] + ", eixo y: " + eixo_y[1] + "] [ Ponto C -" + " eixo x: " + eixo_x[2] + ", eixo y: " + eixo_y[2] + "] [ Ponto D: " + " eixo x: " + eixo_x[3] + ", eixo y: " + eixo_y[3] + " ]";
            document.getElementById("quadrado").setAttribute("points", squ_srt);
            console.log("debug: " + squ_srt_debug);
        }

    } else if (direcao === "esquerda") {
        if (eixo_x[0] > 0 && eixo_x[3] > 0) {
            for (var i = 0; i < eixo_y.length; i++) {
                eixo_x[i] = eixo_x[i] - velocidade;
            }
            var squ_srt = eixo_x[0] + " " + eixo_y[0] + ", " + " " + eixo_x[1] + " " + eixo_y[1] + ", " + " " + eixo_x[2] + " " + eixo_y[2] + ", " + " " + eixo_x[3] + " " + eixo_y[3];
            var squ_srt_debug = "[ Ponto A - eixo x: " + eixo_x[0] + ", eixo y: " + eixo_y[0] + "]  [ Ponto B -" + " eixo x: " + eixo_x[1] + ", eixo y: " + eixo_y[1] + "] [ Ponto C -" + " eixo x: " + eixo_x[2] + ", eixo y: " + eixo_y[2] + "] [ Ponto D: " + " eixo x: " + eixo_x[3] + ", eixo y: " + eixo_y[3] + " ]";
            document.getElementById("quadrado").setAttribute("points", squ_srt);
            console.log("debug: " + squ_srt_debug);
        }

    } else if (direcao === "direita") {
        if (eixo_x[1] < 300 && eixo_x[2] < 300) {
            for (var i = 0; i < eixo_y.length; i++) {
                eixo_x[i] = eixo_x[i] + velocidade;
            }
            var squ_srt = eixo_x[0] + " " + eixo_y[0] + ", " + " " + eixo_x[1] + " " + eixo_y[1] + ", " + " " + eixo_x[2] + " " + eixo_y[2] + ", " + " " + eixo_x[3] + " " + eixo_y[3];
            var squ_srt_debug = "[ Ponto A - eixo x: " + eixo_x[0] + ", eixo y: " + eixo_y[0] + "]  [ Ponto B -" + " eixo x: " + eixo_x[1] + ", eixo y: " + eixo_y[1] + "] [ Ponto C -" + " eixo x: " + eixo_x[2] + ", eixo y: " + eixo_y[2] + "] [ Ponto D: " + " eixo x: " + eixo_x[3] + ", eixo y: " + eixo_y[3] + " ]";
            document.getElementById("quadrado").setAttribute("points", squ_srt);
            console.log("debug: " + squ_srt_debug);
        }

    } else if (direcao === "zero") {
        document.getElementById("sq_velocidade").value = 1;
        console.log("Velocidade definida como 1");

    }
}
function aparencia() {
    document.getElementById("quadrado").setAttribute("stroke", document.getElementById("sq_cores_borda").value);
    document.getElementById("quadrado").setAttribute("stroke-width", document.getElementById("sq_espessura_borda").value);
    document.getElementById("quadrado").setAttribute("fill", document.getElementById("sq_cores").value);
    document.getElementById("view").style.background = document.getElementById("sq_bg").value;
}
function efeito() {
    var circulos;
    if (document.getElementById("sq_efeitos").value === "bolhas") {
        ef_bolhas = setInterval(caos, 100);
    } else if (document.getElementById("sq_efeitos").value === "nenhum") {
        clearInterval(ef_bolhas);
        circulos = document.getElementsByClassName("circulo_voador");
        for (var i = 0; i < circulos.length; i++) {
            circulos[i].remove();
        }
    }
}
function bolhas() {
    var ale_bolha = parseInt(document.getElementById("sq_aleatorio").value);
    if (ale_bolha === 1) {
        var opa_bolha = document.getElementById("sq_opacidade").value;
        var esp_borda = document.getElementById("sq_borda_bol").value;
        var cor_bolha = document.getElementById("sq_cor_bolha").value;
    } else if (ale_bolha === 2) {
        var opa_bolha = (Math.floor(Math.random() * 8) + 1);
        var esp_borda = (Math.floor(Math.random() * 4) + 1);
        var cor_bolha = "rgba(" + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ")";
        for(;conta < 1; conta++){
            document.getElementById("view").style.background = "rgba(" + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ")";
        }
    }
    var circulos = document.getElementsByClassName("circulo_voador");
    if (circulos.length < (Math.floor(Math.random() * 10) + 1)) {
        var circulos = Math.floor(Math.random() * 10);
        for (var j = 0; j < circulos; j++) {
            var x_inicial = (Math.floor(Math.random() * 300) + 1);
            //var y_inicial = Math.floor(Math.random() * 150);
            var y_inicial = 200;
            var r_inicial = (Math.floor(Math.random() * 40) + 1);
            var tag_inicial = "<circle cx=" + x_inicial + " cy=" + y_inicial + " r=" + r_inicial + " class=\"circulo_voador\" stroke=\"white\" stroke-width=\"" + esp_borda + "\" fill=\"" + cor_bolha + "\" opacity=\"0." + opa_bolha + "\" />";
            document.getElementById("view").innerHTML += tag_inicial;
        }
    }
}
function movecircles() {
    var circulos = document.getElementsByClassName("circulo_voador");
    for (var i = 0; i < circulos.length; i++) {
        var y_atual = parseInt(circulos[i].getAttribute("cy"));
        var r_atual = parseInt(circulos[i].getAttribute("r"));
        y_atual = y_atual - (r_atual / 2);
        circulos[i].setAttribute("cy", y_atual);
    }
}
function destruir() {
    var circulos = document.getElementsByClassName("circulo_voador");
    for (var i = 0; i < circulos.length; i++) {
        var y_atual = circulos[i].getAttribute("cy");
        if (parseInt(y_atual) <= 0) {
            circulos[i].remove();
        }
    }
}
function caos() {
    bolhas();
    movecircles();
    destruir();
}